Rails.application.routes.draw do

  root 'data#index'
  get  'result',  to: 'data#result'
  get  'stat',    to: 'data#stat'
  post 'data',    to: 'data#data'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class AddParamsToDatum < ActiveRecord::Migration[5.1]
  def change
    add_column :data, :get_params, :string, {limit: 128}
  end
end

class CreateData < ActiveRecord::Migration[5.1]
  def change
    create_table :data do |t|
      t.string :answer0
      t.string :answer1
      t.string :answer2
      t.string :answer3
      t.string :answer4
      t.string :answer5
      t.string :answer6
      t.string :answer7
      t.string :answer8
      t.string :answer9
      t.string :answer10
      t.string :answer11
      t.string :answer12
      t.string :answer13
      t.string :answer14
      t.string :name
      t.string :number
      t.string :lang

      t.timestamps
    end
  end
end

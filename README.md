# README

Инструкция:

После клона репозитория в каталоге приложения:

#### Установить все необходимые гемы (плагины)
```bundle```

#### Миграция базы данных
```rake db:migrate RAILS_ENV=production```

#### генерация секретного ключа для прилож.
``` rake secret ```

#### Сгенерированный ключ сюда:
config/secrets.yml
```
 production:
  secret_key_base: _secret_key_
```

#### Подстроить приложение под сервер:
```
Passengerfile.json
{
  "environment": "production",
  "address": "127.0.0.1",
  "port": 3000,
  "daemonize": true,
  "user": "ampersand",
  "load_shell_envvars": true
}
```
#### Запустить
```rvmsudo passenger start```


## nginx:
#### Просто проксировать
```
server {
        listen 80;
        server_name www.foo.com;

        # Tells Nginx to serve static assets from this directory.
        root /webapps/foo/public;

        location / {
            # Tells Nginx to forward all requests for www.foo.com
            # to the Passenger Standalone instance listening on port 4000.
            proxy_pass http://127.0.0.1:4000;

            # These are "magic" Nginx configuration options that
            # should be present in order to make the reverse proxying
            # work properly. Also contains some options that make WebSockets
            # work properly with Passenger Standalone. Please learn more at
            # http://nginx.org/en/docs/http/ngx_http_proxy_module.html
            proxy_http_version 1.1;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_buffering off;
        }
    }
```

#### Это для запуска приложения вместе с сервером
```
sudo chmod +x /etc/rc.local
$ sudo nano /etc/rc.local
```
```
#!/bin/sh
cd /home/ampersand/ks-quiz
bundle exec passenger start
```
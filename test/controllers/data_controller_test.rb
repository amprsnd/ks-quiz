require 'test_helper'

class DataControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get data_index_url
    assert_response :success
  end

  test "should get data" do
    get data_data_url
    assert_response :success
  end

  test "should get result" do
    get data_result_url
    assert_response :success
  end

end

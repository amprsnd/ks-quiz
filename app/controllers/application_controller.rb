class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  config.generators do |g|
    g.template_engine :slim
  end

end

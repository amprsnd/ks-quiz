class DataController < ApplicationController

  layout false

  http_basic_authenticate_with name: 'results', password: 'DuOWa5Ni' , except: [:index, :data]

  def index

    #render plain: 'all ok'

  end

  def data

    null = ''
    request.body.rewind
    result = JSON.parse request.body.read
    result = JSON.pretty_generate result
    result = JSON.parse result

    @result = Datum.new

    result.each do |key, value|

      if key == 'answer15'

        @result['name']   = value[0]
        @result['number'] = value[1]
        @result['lang']   = value[2]

      elsif key == 'get_params'
        @result[key] = value.truncate(128, omission: '')
      else

        @result[key] = value

      end

    end

    if @result.save
      render plain: 'ok'
    else
      render plain: @result.errors.first[1]
    end


  end

  def result

    @result = Datum.all

    #render format: [:html, :json]

    respond_to do |format|
      format.html
      format.json
      format.xls
    end

  end

  def stat
    # nothing
  end

end

module DataHelper

  def single_value key, value

    if value.class == String

      value = value.split('"')
      result = ("\"#{key}\": \"#{value[1]}\"").html_safe

    elsif value.nil?

      result = ("\"#{key}\": \"\"").html_safe

    else

      result = ("\"#{key}\": \"#{value}\"").html_safe

    end

    result

  end

  def xls_single_value value

    result = ''

    unless value.nil?

      if value.class == String

        value = value.split('"')
        result = value[1].html_safe

      else

        result = value.html_safe

      end

    end

    result

  end


  def multi_value key, value

    if value.nil?

      result = ("\"#{key}\": \"\"").html_safe

    else

      value = value.gsub('[', '').gsub(']', '').split(', ')
      result = ''

      value.each_with_index do |v, i|

        unless v == 'nil' || v.nil?

          ( value.length - 1) == i ? result += "#{v.gsub('"', '')}" : result += "#{v.gsub('"', '')}, "

        end

        #puts result

      end

      result = ("\"#{key}\": \"#{result}\"").html_safe

    end

    result

  end

  def xls_multi_value value

    if value.nil?

      result = ''

    else

      value = value.gsub('[', '').gsub(']', '').split(', ')
      result = ''

      value.each_with_index do |v, i|

        unless v == 'nil' || v.nil?

          ( value.length - 1) == i ? result += "#{v.gsub('"', '')}" : result += "#{v.gsub('"', '')}, "

        end

        #puts result

      end

      result = result.html_safe

    end

    result

  end

  def formatted_value key , value

    ("\"#{key}\": \"#{value}\"").html_safe

  end

  def coma(index, result)

    (result - 1) == index ? '' : ','

  end

  def get_params_format value

    result = "\"get_params\": \"\""

    unless value.nil?

      result =  "\"get_params\": \"#{URI::decode(value)}\""

    end

    result.html_safe

  end

  def xls_get_params value

    result = ''

    unless value.nil?

      result =  URI::decode(value)

    end

    result.html_safe

  end

end
